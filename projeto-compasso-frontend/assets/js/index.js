window.onload = function (){
    const btnBusca = document.getElementById("btnBusca");
    const divResult = document.getElementById("divResult");
    const btnRepos = document.getElementById("btnRepos");
    const btnReposVis = document.getElementById("btnReposVis");
    btnBusca.addEventListener("click",getBuscaUsuario)
    btnRepos.addEventListener("click",getRepositorio)
    btnReposVis.addEventListener("click",getRepositoriosVisitados)
    async function getBuscaUsuario(){
        document.getElementById("divResult").innerHTML="";
        $(".table").attr('style','display:none');
        $(".repositorio").attr('style','display:none');
        var nome = document.getElementById('nome').value;
        const url = "https://api.github.com/users/" + nome
        const response = await fetch(url)
        const result = await response.json()
        console.log(result);
        const usuario = document.createElement("h4")
        const login = document.createElement("h4")
        const id = document.createElement("h4")
        const company = document.createElement("h4")
        const created_at = document.createElement("h4")
        id.textContent = "ID : " + result.id;
        usuario.textContent = "Nome : " + result.name;
        login.textContent = "Login : " + result.login;
        company.textContent = "Company : " + result.company;
        created_at.textContent = "Criado em : " + result.created_at;
        var image = new Image(200,200);
        image.src = result.avatar_url;
        divResult.appendChild(id)
        divResult.appendChild(usuario)
        divResult.appendChild(login)
        divResult.appendChild(company)
        divResult.appendChild(image)
        $(".resultado").attr("style","margin-left: 18px;");
        $("#btnRepos").attr("style","display:block");
        $("#btnReposVis").attr("style","display:block");
    }
    async function getRepositorio(){
        document.getElementById("table").innerHTML="";
        $(".repositorio-visitados").attr('style','display:none');
        $(".table").attr('style','display:block');
        $(".repositorio").attr('style','display:block');
        const tab = document.getElementById("table");
        var nome = document.getElementById('nome').value;
        const urlRepos = "https://api.github.com/users/"+nome+"/repos"
        const responseRepos = await fetch(urlRepos)
        const resultRepos = await responseRepos.json()
        console.log(resultRepos);
        resultRepos.forEach(i=>{
            let tr = document.createElement('tr');
            let tdName = document.createElement('td');
            let tdID = document.createElement('td');
            let tdUrl = document.createElement('td');
            let linkUrl = document.createElement('a');
            linkUrl.href = i.html_url;
            linkUrl.textContent = i.html_url;
            let tdTextName = document.createTextNode(i.name);
            let tdTextID = document.createTextNode(i.id);
            let tdTextUrl = document.createTextNode(i.html_url);
            let tdElementName = tdName.appendChild(tdTextName);
            let tdElementID = tdID.appendChild(tdTextID);
            let tdElementUrl = tdUrl.appendChild(linkUrl);
            tr.appendChild(tdID);
            tr.appendChild(tdName);
            tr.appendChild(tdUrl);
            tab.appendChild(tr);
        });
    }
    async function getRepositoriosVisitados(){
        document.getElementById("table").innerHTML="";
        $(".table").attr('style','display:block');
        $(".repositorio-visitados").attr('style','display:block');
        const tab = document.getElementById("table");
        var nome = document.getElementById('nome').value;
        const urlRepos = "https://api.github.com/users/"+nome+"/starred"
        const responseRepos = await fetch(urlRepos)
        const resultRepos = await responseRepos.json()
        console.log(resultRepos);
        resultRepos.forEach(i=>{
            let tr = document.createElement('tr');
            let tdName = document.createElement('td');
            let tdID = document.createElement('td');
            let tdUrl = document.createElement('td');
            let linkUrl = document.createElement('a');
            linkUrl.href = i.html_url;
            linkUrl.textContent = i.html_url;
            let tdTextName = document.createTextNode(i.name);
            let tdTextID = document.createTextNode(i.id);
            let tdTextUrl = document.createTextNode(i.html_url);
            let tdElementName = tdName.appendChild(tdTextName);
            let tdElementID = tdID.appendChild(tdTextID);
            let tdElementUrl = tdUrl.appendChild(linkUrl);
            tr.appendChild(tdID);
            tr.appendChild(tdName);
            tr.appendChild(tdUrl);
            tab.appendChild(tr);
        });
    }
}